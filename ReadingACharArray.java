import java.util.Scanner;
public class chars{

	//This method here reads a character at a time using Scanner class
	public static char[] readChar(){
		Scanner in=new Scanner(System.in);
		char[] a=new char[100];
		int i=0;
		do{
			System.out.println("Do you wanna enter a character? y/n");
			char choice=in.next().charAt(0);
			if(choice=='y'||choice=='Y'){
				a[i]=in.next().charAt(0);
				i++;
			}else{
				break;
			}
			
		}while(a[i]!=10);
		printChar(a);
		return a;
	}
	//To print the character array
	public static void printChar(char[] a){
		for(char b:a)
			System.out.print(b+"");
	}
}