import java.io.Serializable;
class def implements Serializable 
{
	int a;
	def(int a)
	{
		this.a=a;
	}
	public String toString() {
		return " num ="+this.a ;
	}

}
class abc implements Serializable {

 	int a=0;
 	String name;
 	int age;
 	String gender;
 	def d ;	//compostion

 	abc( String name, int age , String gender, int i)
 	{

 		this.name = name;
 		this.age = age;
 		this.gender = gender;
 		d = new def(i);
 		
 	}

 	public String toString() {
		return "Name=" + this.name + " Age=" + this.age + " Gender=" + this.gender+ d.toString();
	}

}