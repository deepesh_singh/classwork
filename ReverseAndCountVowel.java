public class Reverse{
	//Method to reverse the string
	public String revStr(String s){
		String a="";
		int count=0;
		for(int i=s.length()-1;i>=0;i--){
			a=a+s.charAt(i);
		return a;
	}
	//Method to count vowels in the String
	public int countVowel(String s){
		int count=0;
		for(int i=s.length()-1;i>=0;i--){
			if(s.charAt(i)=='a'||s.charAt(i)=='A'||s.charAt(i)=='e'||s.charAt(i)=='E'||s.charAt(i)=='i'||
			s.charAt(i)=='I'||s.charAt(i)=='o'||s.charAt(i)=='O'||s.charAt(i)=='u'||s.charAt(i)=='U')
				{count++;}
			}
		return count;
	}
}