@author Deepesh
import java.io.*;
public class FileTest{
	public static void main(String[] args){
	    //Demonstrating The Serialization of Object
        Customer c=new Customer(90,34,23);  //An User defined Class' Object
		try {
			FileOutputStream fos = new FileOutputStream("CustomerObject.ser");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			// write object to file
			oos.writeObject(c);
			System.out.println("Done");
			// closing resources
			oos.close();
			fos.close();
			//Reading the Serialized Object from The file
			FileInputStream fis = new FileInputStream("CustomerObject.ser");
			ObjectInputStream ois = new ObjectInputStream(fis);
			System.out.println(ois.readObject());
			ois.close();
			fis.close();
		} catch (IOException e) {
			e.printStackTrace();
		}	
		catch (ClassNotFoundException cnfe) {
			cnfe.printStackTrace();
		}
	}
}