@author Deepesh
import java.sql.*;  
public class DBconnect{
	public static void main(String args[]){  
	try{  
		//loading the driver class into JRE
		Class.forName("oracle.jdbc.driver.OracleDriver");  
		Connection con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521/orcl","scott","tiger"); 
		//create the statement object  Using Connection Class' Object
		Statement stmt=con.createStatement();  
		//executing the query  
		ResultSet rs=stmt.executeQuery("select * from customer");  
		while(rs.next())  
		System.out.println(rs.getString(1)+"  "+rs.getInt(2)+"  "+rs.getInt(3));  
		  
		//closing the connection object  
		con.close();  
		  
	}catch(Exception e){ System.out.println(e);}  
	  
	}  
}