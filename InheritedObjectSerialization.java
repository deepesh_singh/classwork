@author Deepesh
//Java program to serialise and deserialise a Object
//In case of inheritance and composition

import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
public class InheritedObjectSerialization {
	
	public static void main(String[] args) {


		xyz obj = new xyz(10,345,"luh", 22,"male",1);
		//Serializing the Object in a file
		try {
			
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("Serial.txt")); //writing to a text file
			oos.writeObject(obj);
			System.out.println("Success");
			oos.close();
			//fos.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

		//Deserializing the Object
		try {

			ObjectInputStream ois = new ObjectInputStream(new FileInputStream("text.txt")); 	//Reading from a text file

			abc obj2 = (abc) ois.readObject();
			ois.close();
			System.out.println(obj2.toString());
		} 
		catch (ClassNotFoundException | IOException e) {
			e.printStackTrace();
		}
	}
	
}