@author Deepesh
import java.util.*;
class TestRun{
	public static void main(String[] args) {
		//implementing the priority queue of String 
		PriorityQueue<String> queue=new PriorityQueue<String>();  
        //Going throught the methods of the priority Queue
		queue.add("A"); 
		queue.add("B");  
		queue.add("C");  
		queue.add("D");  
		queue.add("A");  
		System.out.println(queue);
		//checking how poll method works
		System.out.println("head before poll:"+queue.element()); 
		queue.poll();  
		System.out.println("head after poll :"+queue.element()); 
		queue.poll();  
		System.out.println("head after poll :"+queue.element()); 
		queue.poll();  
		System.out.println("head after poll :"+queue.element()); 
		queue.poll();  
		System.out.println("head after poll :"+queue.element()); 
		queue.poll();  
		System.out.println("head after poll :"+queue.element()); 
		System.out.println(queue);

		System.out.println("head:"+queue.element());  
		System.out.println("head:"+queue.peek());  
		System.out.println("iterating the queue elements:");  
		Iterator itr=queue.iterator();  
		while(itr.hasNext()){  
		System.out.println(itr.next());  
		}  
		//other method's implementation
		System.out.println("head before remove:"+queue.element()); 
		//queue.remove();  
		System.out.println("head before poll:"+queue.element()); 
		queue.poll();  
		System.out.println("head after poll :"+queue.element()); 
		System.out.println("after removing two elements:");  
		Iterator<String> itr2=queue.iterator();  
		while(itr2.hasNext()){  
		System.out.println(itr2.next());  
		}
	}
}
}