@author Deepesh
public class Actual{	
	
	public static void main(String[] args){
        Customer c1=new Customer(12,34,56);
		Customer c2=new Customer(1,3,5);
		Customer c3=new Customer(4,6,7);

		Customer[] c={c1,c2,c3};
		try{
			ArrayTest.deleteElement(c,2);
			for(Customer cust:c){
				try{
					System.out.println(cust.toString());
				}
				catch(NullPointerException npe){
					System.out.println("Object is Null.");
				}
				catch(Exception ex){
					System.out.println("Something's not right."+ex.getMessage());
				}
			}
		}
		catch(ArrayIndexOutOfBoundsException aiob){
			System.out.println("Given index size exceeds array size.");
		}
		catch(NegativeArraySizeException nas){
			System.out.println("Negative index size passed.");
		}
		catch(NullPointerException npe){
			System.out.println("Object is Null.");
		}
		catch(Exception e){
			System.out.println(e.getMessage());
		}
	}
}	